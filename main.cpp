#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <cmath>

#define PI 3.14159265

using namespace std;

const int screen_x= 800, screen_y= 600;
sf::RenderWindow window(sf::VideoMode(screen_x, screen_y), "Window");

struct car_struct
{
	float length;
	float breadth;
	float left_edge;
	float top_edge;
	float right_edge;
	float bottom_edge;
	float x;
	float y;
	float speed;
	float direction;
	sf::Texture texture;
	sf::Sprite sprite;
	sf::SoundBuffer rev_buffer;
	sf::Sound rev_sound; 
} black_car, blue_car, red_car, green_car;

struct bullet_struct
{
	float length;
	float breadth;
	float left_edge;
	float top_edge;
	float right_edge;
	float bottom_edge;
	float x;
	float y;
	float speed;
	float direction;
	bool collision;
	sf::Texture texture;
	sf::Sprite sprite;
	sf::SoundBuffer shoot_buffer;
	sf::Sound shoot_sound;
	sf::Time time;
} blue_bullet, red_bullet, green_bullet[2];

struct explosion_struct
{
	sf::Texture texture;
	sf::Sprite sprite;
} small_explosion, medium_explosion, large_explosion;

void initialize_cars()
{
	black_car.length= 35;
	black_car.breadth= 15;
	black_car.x= 100;
	black_car.y= 100;
	black_car.speed= 0.7;
	black_car.texture.loadFromFile("black_car.png");
	black_car.sprite.setTexture(black_car.texture);
	black_car.sprite.setPosition(black_car.x, black_car.y);
	black_car.sprite.setOrigin(black_car.length/2, black_car.breadth/2);
	black_car.rev_buffer.loadFromFile("rev.wav");
	black_car.rev_sound.setBuffer(black_car.rev_buffer);
	
	blue_car.length= 35;
	blue_car.breadth= 15;
	blue_car.x= 200;
	blue_car.y= 200;
	blue_car.speed= 0.5;
	blue_car.texture.loadFromFile("blue_car.png");
	blue_car.sprite.setTexture(blue_car.texture);
	blue_car.sprite.setPosition(blue_car.x, blue_car.y);
	blue_car.sprite.setOrigin(blue_car.length/2, blue_car.breadth/2);
	blue_car.rev_buffer.loadFromFile("rev.wav");
	blue_car.rev_sound.setBuffer(blue_car.rev_buffer);

	red_car.length= 35;
	red_car.breadth= 15;
	red_car.x= 300;
	red_car.y= 300;
	red_car.speed= 0.4;
	red_car.texture.loadFromFile("red_car.png");
	red_car.sprite.setTexture(red_car.texture);
	red_car.sprite.setPosition(red_car.x, red_car.y);
	red_car.sprite.setOrigin(red_car.length/2, red_car.breadth/2);
	red_car.rev_buffer.loadFromFile("rev.wav");
	red_car.rev_sound.setBuffer(red_car.rev_buffer);
	
	green_car.length= 35;
	green_car.breadth= 15;
	green_car.x= 400;
	green_car.y= 400;
	green_car.speed= 0.3;
	green_car.texture.loadFromFile("green_car.png");
	green_car.sprite.setTexture(green_car.texture);
	green_car.sprite.setPosition(green_car.x, green_car.y);
	green_car.sprite.setOrigin(green_car.length/2, green_car.breadth/2);
	green_car.rev_buffer.loadFromFile("rev.wav");
	green_car.rev_sound.setBuffer(green_car.rev_buffer);
}

void initialize_bullets()
{
	blue_bullet.length= 20;
	blue_bullet.breadth= 10;
	blue_bullet.x= -1;
	blue_bullet.y= -1;
	blue_bullet.speed= 1.2;
	blue_bullet.collision= false;
	blue_bullet.texture.loadFromFile("blue_bullet.png");
	blue_bullet.sprite.setTexture(blue_bullet.texture);
	blue_bullet.sprite.setPosition(blue_bullet.x, blue_bullet.y);
	blue_bullet.sprite.setOrigin(blue_bullet.length/2, blue_bullet.breadth/2);
	blue_bullet.shoot_buffer.loadFromFile("shoot.wav");
	blue_bullet.shoot_sound.setBuffer(blue_bullet.shoot_buffer);
	
	red_bullet.length= 20;
	red_bullet.breadth= 10;
	red_bullet.x= -1;
	red_bullet.y= -1;
	red_bullet.speed= 1.5;
	red_bullet.collision= false;
	red_bullet.texture.loadFromFile("red_bullet.png");
	red_bullet.sprite.setTexture(red_bullet.texture);
	red_bullet.sprite.setPosition(red_bullet.x, red_bullet.y);
	red_bullet.sprite.setOrigin(red_bullet.length/2, red_bullet.breadth/2);
	red_bullet.shoot_buffer.loadFromFile("shoot.wav");
	red_bullet.shoot_sound.setBuffer(red_bullet.shoot_buffer);
	
	for(int i= 0; i<2; i++)
	{
		green_bullet[i].length= 20;
		green_bullet[i].breadth= 10;
		green_bullet[i].x= -1;
		green_bullet[i].y= -1;
		green_bullet[i].speed= 1.0;
		green_bullet[i].collision= false;
		green_bullet[i].texture.loadFromFile("green_bullet.png");
		green_bullet[i].sprite.setTexture(green_bullet[i].texture);
		green_bullet[i].sprite.setPosition(green_bullet[i].x, green_bullet[i].y);
		green_bullet[i].sprite.setOrigin(green_bullet[i].length/2, green_bullet[i].breadth/2);
		green_bullet[i].shoot_buffer.loadFromFile("shoot.wav");
		green_bullet[i].shoot_sound.setBuffer(green_bullet[i].shoot_buffer);
	}
}

void barrier_detection(car_struct *car)
{
	if(car->sprite.getPosition().x<0)
		car->sprite.setPosition(0, car->sprite.getPosition().y);
	if(car->sprite.getPosition().y<0)
		car->sprite.setPosition(car->sprite.getPosition().x, 0);
	if(car->sprite.getPosition().x>screen_x)
		car->sprite.setPosition(screen_x, car->sprite.getPosition().y);
	if(car->sprite.getPosition().y>screen_y)
		car->sprite.setPosition(car->sprite.getPosition().x, screen_y);
}

void move_black_car()
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		black_car.direction= black_car.sprite.getRotation();
		black_car.sprite.move(black_car.speed*cos(black_car.direction*PI/180), black_car.speed*sin(black_car.direction*PI/180));
		black_car.rev_sound.play();
	}
	
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		black_car.direction= black_car.sprite.getRotation();
		black_car.sprite.move(-black_car.speed*cos(black_car.direction*PI/180), -black_car.speed*sin(black_car.direction*PI/180));
		black_car.rev_sound.play();
	}
	
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		black_car.sprite.rotate(-0.5);			
		
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		black_car.sprite.rotate(0.5);
}

void track_black_car(car_struct *player_car, car_struct *ai_car)
{
	if((player_car->sprite.getPosition().x)-(ai_car->sprite.getPosition().x)<0)
		ai_car->sprite.setRotation(180+atan(((player_car->sprite.getPosition().y)-(ai_car->sprite.getPosition().y))/((player_car->sprite.getPosition().x)-(ai_car->sprite.getPosition().x)))*180/PI);
		
	if((player_car->sprite.getPosition().x)-(ai_car->sprite.getPosition().x)>0)
		ai_car->sprite.setRotation(atan(((player_car->sprite.getPosition().y)-(ai_car->sprite.getPosition().y))/((player_car->sprite.getPosition().x)-(ai_car->sprite.getPosition().x)))*180/PI);
}

void move_ai_car()
{
	track_black_car(&black_car, &blue_car);
	blue_car.direction= blue_car.sprite.getRotation();
	blue_car.sprite.move(blue_car.speed*cos(blue_car.direction*PI/180), blue_car.speed*sin(blue_car.direction*PI/180));
	blue_car.rev_sound.play();
	
	track_black_car(&black_car, &red_car);
	red_car.direction= red_car.sprite.getRotation();
	red_car.sprite.move(red_car.speed*cos(red_car.direction*PI/180), red_car.speed*sin(red_car.direction*PI/180));
	red_car.rev_sound.play();
	
	track_black_car(&black_car, &green_car);
	green_car.direction= green_car.sprite.getRotation();
	green_car.sprite.move(green_car.speed*cos(green_car.direction*PI/180), green_car.speed*sin(green_car.direction*PI/180));
	green_car.rev_sound.play();
}

void move_cars()
{
	barrier_detection(&black_car);
	move_black_car();
	
	move_ai_car();
}

void display_cars()
{
	window.draw(black_car.sprite);
	window.draw(blue_car.sprite);
	window.draw(red_car.sprite);
	window.draw(green_car.sprite);
}

void set_blue_bullet()
{
	blue_bullet.x= blue_car.sprite.getPosition().x;
	blue_bullet.y= blue_car.sprite.getPosition().y;
	blue_bullet.sprite.setPosition(blue_bullet.x, blue_bullet.y);
	blue_bullet.direction= blue_car.sprite.getRotation();
	blue_bullet.sprite.setRotation(blue_bullet.direction);
	blue_bullet.shoot_sound.play();
}

void set_red_bullet()
{
	red_bullet.x= red_car.sprite.getPosition().x;
	red_bullet.y= red_car.sprite.getPosition().y;
	red_bullet.sprite.setPosition(red_bullet.x, red_bullet.y);
	red_bullet.direction= red_car.sprite.getRotation();
	red_bullet.sprite.setRotation(red_bullet.direction);
	red_bullet.shoot_sound.play();
}

void set_green_bullet()
{
	for(int i= 0; i<2; i++)
	{
		green_bullet[i].x= green_car.sprite.getPosition().x;
		green_bullet[i].y= green_car.sprite.getPosition().y;
		green_bullet[i].sprite.setPosition(green_bullet[i].x, green_bullet[i].y);
		red_bullet.shoot_sound.play();
	}
	green_bullet[0].direction= green_car.sprite.getRotation()- 45;
	green_bullet[1].direction= green_car.sprite.getRotation()+ 45;
	
	green_bullet[0].sprite.setRotation(green_bullet[0].direction);
	green_bullet[1].sprite.setRotation(green_bullet[1].direction);
}

void set_bullet_timer(sf::Clock *clock)
{
	sf::Time now= clock->getElapsedTime();
	
    if(now- blue_bullet.time> sf::seconds(1.5))
    {
        set_blue_bullet();
        blue_bullet.time= now;
    }
    	
    if(now- red_bullet.time> sf::seconds(1.75))
    {
    	set_red_bullet();
    	red_bullet.time= now;
    }
    
    if(now- green_bullet[0].time> sf::seconds(2.00))
    {
        set_green_bullet();
        green_bullet[0].time= now;
    }
}

void move_bullets()
{
	blue_bullet.sprite.move(blue_bullet.speed*cos(blue_bullet.direction*PI/180), blue_bullet.speed*sin(blue_bullet.direction*PI/180));
	red_bullet.sprite.move(red_bullet.speed*cos(red_bullet.direction*PI/180), red_bullet.speed*sin(red_bullet.direction*PI/180));
	for(int i=0; i<2; i++)
		green_bullet[i].sprite.move(green_bullet[i].speed*cos(green_bullet[i].direction*PI/180), green_bullet[i].speed*sin(green_bullet[i].direction*PI/180));
}

void display_bullets()
{
	window.draw(blue_bullet.sprite);
	window.draw(red_bullet.sprite);
	for(int i=0; i<2; i++)
		window.draw(green_bullet[i].sprite);	
}

void initialize_explosion_sprite()
{
	small_explosion.texture.loadFromFile("explosion_small.png");
	small_explosion.sprite.setTexture(small_explosion.texture);
	
	medium_explosion.texture.loadFromFile("explosion_medium.png");
	medium_explosion.sprite.setTexture(medium_explosion.texture);
	
	large_explosion.texture.loadFromFile("explosion_large.png");
	large_explosion.sprite.setTexture(large_explosion.texture);
}

void explosion_animation(explosion_struct explosion)
{
	explosion.sprite.setPosition(black_car.sprite.getPosition());
	for(int i=0; i<500; i++)
	{
		window.clear(sf::Color::White);
		window.draw(explosion.sprite);
		window.display();
	}
}

void explosion_sprite()
{
	sf::SoundBuffer explosion_buffer;
	explosion_buffer.loadFromFile("explosion.wav");
	sf::Sound explosion_sound;
	explosion_sound.setBuffer(explosion_buffer);
	explosion_animation(small_explosion);
	explosion_sound.play();
	explosion_animation(medium_explosion);
	explosion_animation(large_explosion);
}

bool bullet_collision(sf::Clock *clock, car_struct *player_car, bullet_struct *ai_bullet)
{
	player_car->left_edge= player_car->sprite.getPosition().x- player_car->length/2;
	player_car->top_edge= player_car->sprite.getPosition().y- player_car->breadth/2;
	player_car->right_edge= player_car->sprite.getPosition().x+ player_car->length/2;
	player_car->bottom_edge= player_car->sprite.getPosition().y+ player_car->breadth/2;
	
	ai_bullet->left_edge= ai_bullet->sprite.getPosition().x- ai_bullet->length/2;
	ai_bullet->top_edge= ai_bullet->sprite.getPosition().y- ai_bullet->breadth/2;
	ai_bullet->right_edge= ai_bullet->sprite.getPosition().x+ ai_bullet->length/2;
	ai_bullet->bottom_edge= ai_bullet->sprite.getPosition().y+ ai_bullet->breadth/2;
	
	if(ai_bullet->right_edge< player_car->left_edge || ai_bullet->left_edge> player_car->right_edge || ai_bullet->top_edge> player_car->bottom_edge || ai_bullet->bottom_edge< player_car->top_edge)
		return false;
		
	cout<<"bullet collided"<<endl;
	cout<<"Time Elapsed: "<<clock->getElapsedTime().asSeconds()<<endl;
	explosion_sprite();
	
	return true;	
}

int main()
{	
	sf::Clock clock;
	bool end_game= false;
	initialize_cars();
 	initialize_bullets();
 	initialize_explosion_sprite();
    while (window.isOpen())
    {	
        sf::Event event;
        if (window.pollEvent(event))
        {
            if (event.type== sf::Event::Closed || (event.type== sf::Event::KeyReleased) && (event.key.code== sf::Keyboard::Escape))
                window.close();
        }
			
        move_cars();
        set_bullet_timer(&clock);
        move_bullets();
        
		end_game= bullet_collision(&clock, &black_car, &blue_bullet);
		if(end_game== true)
        	break;
        	
		end_game= bullet_collision(&clock, &black_car, &red_bullet);
		if(end_game== true)
        	break;
        	
		for(int i=0; i<2; i++)
        {
        	end_game= bullet_collision(&clock, &black_car, &green_bullet[i]);
        	if(end_game== true)
        	break;
        }
        if(end_game== true)
        	break;
    	
		window.clear(sf::Color::White);
		display_cars();
		display_bullets();
        window.display();
    }
    return 0;
}
